package net.metasite.controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang.SystemUtils;
import org.primefaces.model.StreamedContent;

import net.metasite.DataReader;
import net.metasite.DataWriter;

public class DataController {
	private static final Logger log = Logger.getLogger(DataController.class.getName());
	private Map<String, Integer> wordsDuplication = new HashMap<>();
	private DataWriter writer;

	public void init() {
		DataReader reader = new DataReader();
		File dir = null;
		List<String> wordsList = new ArrayList<String>();

		if (SystemUtils.IS_OS_WINDOWS) {
			dir = new File(System.getProperty("user.dir") + "\\textFiles");
			wordsList.addAll(checkIfDirectoryExistAndFindAllTheWords(reader, dir));

		} else if (SystemUtils.IS_OS_LINUX) {
			dir = new File(System.getProperty("user.dir") + "/textFiles");
			wordsList.addAll(checkIfDirectoryExistAndFindAllTheWords(reader, dir));
		}

		wordsDuplication = findWordsDuplicationIn(wordsList);
	}
	
	/**
	 * Checks if default directory exist and then returns all words in directory files.
	 * If directory does not exist, new directory is created.
	 * @param reader - file data reader
	 * @param dir - directory to real all the files
	 * @return words - list in given directory files
	 */
	private List<String> checkIfDirectoryExistAndFindAllTheWords(DataReader reader, File dir) {
		List<String> wordsList = new ArrayList<>();

		if (dir.isDirectory()) {
			wordsList.addAll(reader.readAllWordsInFilesFor(dir.getAbsolutePath()));
			log.info("File directory exist.");
		} else {
			new File(dir.getAbsolutePath()).mkdir();
			log.info("File directory does not exist. New file directory was created.");
		}
		return wordsList;
	}

	private Map<String, Integer> findWordsDuplicationIn(List<String> wordsList) {
		Map<String, Integer> wordsDuplicationMap = new HashMap<>();

		for (String word : wordsList) {
			if (wordsDuplicationMap.containsKey(word)) {
				wordsDuplicationMap.put(word, wordsDuplicationMap.get(word) + 1);// increase duplication count
				
			} else {
				wordsDuplicationMap.put(word, 0);
			}
		}
		return wordsDuplicationMap;
	}

	public StreamedContent generateAtoGFile() {
		return writer.generateFileByRegex(wordsDuplication, "[a-g]+", "A-G");
	}

	public StreamedContent generateHtoNFile() {
		return writer.generateFileByRegex(wordsDuplication, "[h-n]+", "H-N");
	}

	public StreamedContent generateOtoUFile() {
		return writer.generateFileByRegex(wordsDuplication, "[o-u]+", "O-U");
	}

	public StreamedContent generateVtoZFile() {
		return writer.generateFileByRegex(wordsDuplication, "[v-z]+", "V-Z");
	}

	public Map<String, Integer> getWordsDuplication() {
		return wordsDuplication;
	}

	public void setWordsDuplication(Map<String, Integer> wordsDuplication) {
		this.wordsDuplication = wordsDuplication;
	}

	public DataWriter getWriter() {
		return writer;
	}

	public void setWriter(DataWriter writer) {
		this.writer = writer;
	}

}
