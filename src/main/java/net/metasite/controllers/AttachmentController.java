package net.metasite.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.SystemUtils;
import org.primefaces.model.UploadedFile;

public class AttachmentController {
	private static final Logger log = Logger.getLogger(AttachmentController.class.getName());
	private UploadedFile uploadedFile;

	public void upload() {
		String fileName = uploadedFile.getFileName();
		File dir = null;

		if (SystemUtils.IS_OS_WINDOWS) {
			dir = new File(System.getProperty("user.dir") + "\\textFiles");
			fileName = substringRealFileNameFor(fileName);

		} else if (SystemUtils.IS_OS_LINUX) {
			dir = new File(System.getProperty("user.dir") + "/textFiles");
		}
		
		handleFileUpload(fileName, dir);
	}

	private void handleFileUpload(String fileName, File dir) {
		InputStream input = null;
		OutputStream output = null;
		
		try {
			input = uploadedFile.getInputstream();
			output = new FileOutputStream(new File(dir.getAbsolutePath(), fileName));
			IOUtils.copy(input, output);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("New file uploaded."));
			log.info("New file uploaded to: " + dir);

		} catch (FileNotFoundException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("File not found."));
			log.warning("File not found: " + e.getMessage() + ". " + dir.getAbsolutePath() + "\\" + fileName);

		} catch (IOException e) {
			log.warning("An error occurs when uploading the file: " + e.getMessage());

		} finally {
			IOUtils.closeQuietly(input);
			IOUtils.closeQuietly(output);
		}
	}
	
	/**
	 * Get real file name for Windows platform only, due to bad file name
	 * selection(filename = full file directory) when uploading the file.
	 */
	private String substringRealFileNameFor(String fileDirectory) {
		String[] array = fileDirectory.split("\\\\");
		return array[array.length - 1];
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}
}
