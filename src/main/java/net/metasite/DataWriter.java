package net.metasite;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.lang.SystemUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

public class DataWriter {
	private static final Logger log = Logger.getLogger(DataWriter.class.getName());

	/**
	 * Generates file and his content with duplication count by given regex
	 * @param wordsMap words map with duplication count
	 * @param regex regex pattern to filter words
	 * @param fileName file name
	 * @return generated file with words matched the regex
	 */
	public StreamedContent generateFileByRegex(Map<String, Integer> wordsMap, String regex, String fileName) {
		Map<String, Integer> filteredWordsMap = new HashMap<>();

		if (wordsMap != null) {
			for (String word : wordsMap.keySet()) {
				if (word.substring(0, 1).matches(regex)) {
					filteredWordsMap.put(word, wordsMap.get(word));
				}
			}
		}

		return generateFile(filteredWordsMap, fileName);
	}

	private StreamedContent generateFile(Map<String, Integer> duplicatedWordsMap, String title) {
		PrintWriter outputStream = null;
		StreamedContent download = null;
		File defaultDir = null;
		String fileDir = "";
		
		if (SystemUtils.IS_OS_WINDOWS) {
			defaultDir = new File(System.getProperty("user.dir") + "\\" + "outputFiles");
			createNewDirectoryIFNotExist(defaultDir);
			fileDir = defaultDir.getAbsolutePath() + "\\" + title + ".txt";
			
		} else if (SystemUtils.IS_OS_LINUX) {
			defaultDir = new File(System.getProperty("user.dir") + "/outputFiles");
			createNewDirectoryIFNotExist(defaultDir);
			fileDir = defaultDir.getAbsolutePath() + "/" + title + ".txt";
		}
		generateFileContent(duplicatedWordsMap, outputStream, fileDir);
		download = generateDownloadContent(download, fileDir);

		return download;
	}

	private void generateFileContent(Map<String, Integer> duplicatedWordsMap,
			PrintWriter outputStream, String fileDir) {
		
		try {
			outputStream = new PrintWriter(new FileWriter(fileDir));
			for (Entry<String, Integer> value : duplicatedWordsMap.entrySet()) {
				outputStream.write(value.getKey() + " " + value.getValue());
				outputStream.write("\n");
			}
		} catch (IOException e) {
			log.warning("An exception occurs when writing data to file: " + e.getMessage());
		} finally {
			outputStream.close();
		}
	}

	private StreamedContent generateDownloadContent(StreamedContent download, String fileDir) {
		InputStream inputStream;
		ExternalContext externalContext;
		
		try {
			File file = new File(fileDir);
			download = new DefaultStreamedContent();
			inputStream = new FileInputStream(file);
			externalContext = FacesContext.getCurrentInstance().getExternalContext();
			download = new DefaultStreamedContent(inputStream, externalContext.getMimeType(file.getName()),
					file.getName());

		} catch (Exception e) {
			log.warning("An error occurs when generating download file: " + e.getMessage());
		}
		return download;
	}

	private void createNewDirectoryIFNotExist(File dir) {
		if (!dir.isDirectory()) {
			new File(dir.getAbsolutePath()).mkdir();
			log.info("Output directory created.");
		}
	}
}
