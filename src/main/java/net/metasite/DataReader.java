package net.metasite;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataReader implements Runnable {
	private static final Logger log = Logger.getLogger(DataReader.class.getName());
	private static final int THREADS = 8;
	private BufferedReader reader;

	/**
	 * Reads all files and their content in given directory.
	 * 
	 * @param filesDirectory
	 *            directory to read the files.
	 * @return returns only words in given directory files, symbols and numbers
	 *         will be ignored.
	 */
	public List<String> readAllWordsInFilesFor(String filesDirectory) {
		List<String> wordsList = new ArrayList<String>();
		ExecutorService executor = Executors.newFixedThreadPool(THREADS);

		try (Stream<Path> filePathStream = Files.walk(Paths.get(filesDirectory))) {
			filePathStream.forEach(filePath -> {

				if (Files.isRegularFile(filePath)) {
					Runnable worker = new DataReader();
					executor.execute(worker);
					wordsList.addAll(readWordsInFile(filePath));
				}
			});
		} catch (IOException e) {
			log.severe("An error occurs when looking for files in directory: " + e.getMessage());
		}

		// This will make the executor accept no new threads
		// and finish all existing threads in the queue
		executor.shutdown();

		return wordsList.stream().map(String::toLowerCase).collect(Collectors.toList());
	}

	private List<String> readWordsInFile(Path filePath) {
		List<String> wordsList = new ArrayList<String>();
		log.info("Reading the file: " + filePath);

		try {
			reader = new BufferedReader(new FileReader(String.valueOf(filePath)));
			String line = reader.readLine();

			while (line != null && !line.isEmpty()) {
				String[] wordsInLine = line.split(" ");
				filterOnlyWords(wordsList, wordsInLine);
				line = reader.readLine();
			}

		} catch (Exception e) {
			log.severe("An error occurs when reading the file: " + e.getMessage());
		}

		log.info("Words found : " + wordsList.size() + " in file: " + filePath);
		return wordsList;
	}

	private void filterOnlyWords(List<String> wordsList, String[] wordsInLine) {
		for (String word : wordsInLine) {
			if (word.matches("[a-zA-Z]+")) {
				wordsList.add(word);
			}
		}
	}

	@Override
	public void run() {
		String threadName = Thread.currentThread().getName();
		log.info("Running thread: " + threadName);
	}

}
